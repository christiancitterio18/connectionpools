import logging
import time
from threading import Lock
from typing import Optional
from ConnectionPools.db_connection import DBConn


class ConnPool:

    """Class implementing a connection pool object with Lock() threading system."""

    def __init__(self, connection, conn_number=15, max_conn_number=50):
        self.clean_up_clock: time = time.time()
        self.mutex: Lock = Lock()
        self.connection = connection
        self.conn_number: int = conn_number
        self.max_conn_number: int = max_conn_number
        self.conn_pool: list = self.populate_pool()

    def populate_pool(self) -> list:
        """Populate list with 'on-start' DBConn objects."""
        list_conn: list = []
        for i in range(self.conn_number):
            list_conn.append(DBConn(self.connection, i+1))

        return list_conn

    def get_connection(self) -> Optional[DBConn]:
        """Returns connection or None if none are available."""
        try:
            self.mutex.acquire()
            filtered = list(filter(lambda c: c.get_status() == "inactive", self.conn_pool))

            if len(filtered) > 0:
                item: DBConn = filtered[0]
                item.set_status(1)
                return item
            else:
                if self.add_connection():
                    item: DBConn = list(filter(lambda c: c.get_status() == "inactive", self.conn_pool))[0]
                    item.set_status(1)
                    return item

            logging.warning("No connection available! Pool is full!")
            return

        except Exception as e:
            logging.error(f"an exception occurred during run-time: {e}")

        finally:
            if self.mutex:
                self.mutex.release()

    def add_connection(self) -> bool:
        """Add new DBConn to the pool."""
        if len(self.conn_pool) < self.max_conn_number:
            self.conn_pool.append(DBConn(self.connection, len(self.conn_pool)))
            return True

        return False

    def deactivate_connection_and_clean_up(self, item) -> None:
        """Sets connection to inactive."""
        try:
            self.mutex.acquire()
            item.set_status(0)
            self.conn_pool[self.conn_pool.index(item)] = item

        except Exception as e:
            logging.error(f"an exception occurred during run-time: {e}")

        finally:
            if self.mutex:
                self.mutex.release()

        self.clean_up()

    def clean_up(self) -> None:
        """Cleans up excessive connections and errors connection."""
        try:
            self.mutex.acquire()
            errors = list(filter(lambda c: c.get_status() == "error", self.conn_pool))

            for item in errors:
                item.close_connection()
                self.conn_pool.pop(self.conn_pool.index(item))

            if time.time() - self.clean_up_clock >= 60:
                self.clean_up_clock = time.time()
                inactive = list(filter(lambda c: c.get_status() == "inactive", self.conn_pool))
                for item in inactive:
                    if len(self.conn_pool) == self.conn_number:
                        break
                    item.close_connection()
                    self.conn_pool.pop(self.conn_pool.index(item))

            if len(self.conn_pool) < self.conn_number:
                for i in range(len(self.conn_pool) - 1, self.conn_number):
                    self.add_connection()

            logging.info("Clean up is done!")

        except Exception as e:
            logging.error(f"an exception occurred during run-time: {e}")

        finally:
            if self.mutex:
                self.mutex.release()
