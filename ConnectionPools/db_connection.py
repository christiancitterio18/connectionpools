import logging
from typing import Optional

ERROR = -1
INACTIVE = 0
ACTIVE = 1

STATE = {
    ERROR: "error",
    INACTIVE: "inactive",
    ACTIVE: "active"
}


class DBConn(object):

    """Custom connection object for Connection pool."""

    def __init__(self, connection, conn_id, status: int = 0):
        self.connection = connection
        self.conn_id = conn_id
        self.status = status

    def get_status(self) -> str:
        """Gets status value."""
        return STATE[self.status]

    def get_conn_id(self) -> int:
        """Returns the connection number."""
        return self.conn_id

    def set_status(self, value: int) -> None:
        """Changes status."""
        self.status = (value if value in range(-1, 2) else -1)

    def close_connection(self):
        """Closes open connection on clean_up."""
        if self.connection:
            self.connection.close()

    def exec_dql(self, sql) -> Optional[list]:
        """Executes a data query language instruction."""
        try:
            cur = self.connection.cursor()
            cur.execute(sql)
            results: list = list(cur.fetchall())
            return results

        except Exception as e:
            self.set_status(-1)
            logging.error(f"Error during sql execution: {e}")
            return
        finally:
            if cur:
                cur.close()

    def exec_dml(self, sql) -> Optional[bool]:
        """Executes a data manipulation language instruction."""
        try:
            cur = self.connection.cursor()
            cur.execute(sql)
            self.connection.commit()
            return True

        except Exception as e:
            self.set_status(-1)
            logging.error(f"Error during sql execution: {e}")
            return
        finally:
            if cur:
                cur.close()
