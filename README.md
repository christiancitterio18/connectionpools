# ConnectionPools
Side project implementig a connection pool class,  to be used specifically with databases. It give access to standard data manipulation and data reading actions.

## Installation
To use the project into your projects simply [download it](https://gitlab.com/christiancitterio18/connectionpools/-/archive/main/connectionpools-main.zip). 
Once unpacked, you can add it into the project interpreter path, and use it into your project.

Or you can install it whit pip using command 

`pip install git+https://gitlab.com/christiancitterio18/connectionpools`
