import logging
import time
import unittest

import tests.utils.connector as db


class ConnectionTest(unittest.TestCase):

    def test_connection(self):
        self.start_time = time.time()
        conn_n = range(1000)
        for _ in conn_n:
            self.select()

        duration = time.time() - self.start_time
        logging.warning(f"Without connection pool needs {duration} seconds to complete for {len(conn_n)} connections.")

    def select(self) -> None:
        try:
            conn = db.connect()
            cur = conn.cursor()
            cur.execute('SELECT * FROM auth;')
            cur.fetchall()
        except Exception as err:
            logging.error(err)
        finally:
            if conn is not None:
                cur.close()
                conn.close()


if __name__ == '__main__':
    unittest.main()
