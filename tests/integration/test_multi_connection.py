import concurrent.futures
import logging
import time
import unittest

from ConnectionPools.conn_pool import ConnPool
from tests.utils import connector as db


class MyTestCase(unittest.TestCase):

    def setUp(self) -> None:
        self.connection_pool = ConnPool(db.connect())
        self.query = 'SELECT * FROM test;'

    def test_multi_connection(self):
        self.start_time = time.time()
        workers = range(1000)
        with concurrent.futures.ThreadPoolExecutor() as executor:
            executor.map(self.connect_and_select, workers)
        end_time = time.time() - self.start_time
        logging.warning(f"Connection pool needs {end_time} seconds to complete whit {len(workers)} workers.")

    def connect_and_select(self):
        conn = self.connection_pool.get_connection()

        conn.exec_dql(self.query)

        self.connection_pool.deactivate_connection_and_clean_up(conn)


if __name__ == '__main__':
    unittest.main()
