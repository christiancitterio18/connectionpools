import unittest

from tests.utils import connector as db
from ConnectionPools.conn_pool import ConnPool
from ConnectionPools.db_connection import DBConn


class ConnPoolTest(unittest.TestCase):
    def setUp(self) -> None:
        self.pool: ConnPool = ConnPool(db.connect())
        self.pool2: ConnPool = ConnPool(db.connect(), 3, 100)

    def test_populate_pool(self):
        list_conn = self.pool.populate_pool()
        list_conn2 = self.pool2.populate_pool()
        self.assertEqual(len(list_conn), 15)
        self.assertTrue(len(list_conn2), 3)

    def test_add_connection(self):
        self.pool.add_connection()
        self.pool2.add_connection()

        len_list = [len(self.pool.conn_pool), len(self.pool2.conn_pool)]
        self.assertEqual(len_list[0], self.pool.conn_number + 1)
        self.assertEqual(len_list[1], self.pool2.conn_number + 1)

    def test_get_connection(self):
        conn1 = self.pool.get_connection()
        conn2 = self.pool2.get_connection()

        self.assertIsInstance(conn1, DBConn)
        self.assertIsInstance(conn2, DBConn)

    def test_clean_up(self):
        self.pool.clean_up()
        self.pool2.clean_up()

        self.assertEqual(len(self.pool.conn_pool), 15)
        self.assertEqual(len(self.pool2.conn_pool), 3)


if __name__ == '__main__':
    unittest.main()
