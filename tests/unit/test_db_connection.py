import random
import unittest
from tests.utils import connector as db
from ConnectionPools.db_connection import DBConn


class DBConnectionTest(unittest.TestCase):

    def setUp(self) -> None:
        self.connection = db.connect()
        self.db_conn = DBConn(self.connection, 1)

    def test_get_status(self):
        state = "inactive"
        state2 = "error"

        self.assertIsInstance(self.db_conn.get_status(), str)
        self.assertEqual(state, self.db_conn.get_status())
        self.assertNotEqual(state2, self.db_conn.get_status())

    def test_get_conn_id(self):
        dbc_id = self.db_conn.get_conn_id()

        self.assertIsInstance(dbc_id, int)
        self.assertEqual(1, dbc_id)

    def test_set_status(self):
        state = "inactive"
        state2 = "error"

        self.assertEqual(state, self.db_conn.get_status())
        self.assertNotEqual(state2, self.db_conn.get_status())

        self.db_conn.set_status(-1)

        self.assertEqual(state2, self.db_conn.get_status())
        self.assertNotEqual(state, self.db_conn.get_status())

    def test_exec_dql(self):
        sql = "SELECT Version();"
        result = self.db_conn.exec_dql(sql)

        self.assertIsNotNone(result)
        self.assertIsInstance(result, list)

    def test_exec_dml(self):
        text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        sql = f"INSERT INTO test (test) VALUES ('{''.join(random.choices(text + text.lower(), k=5))}');"
        result = self.db_conn.exec_dml(sql)

        self.assertIsNotNone(result)
        self.assertIsInstance(result, bool)
        self.assertTrue(result)


if __name__ == '__main__':
    unittest.main()
