import psycopg2
from tests.utils.config import config


def connect() -> any:
    """
    Creates and return cursor for data manipulation
    :return: any | cursor
    """
    conn: any = None
    try:
        params: dict = config()
        conn = psycopg2.connect(**params)
        return conn
    except (Exception, psycopg2.DatabaseError) as e:
        print('DB connection error: {}'.format(e))
    finally:
        if conn is None:
            return None


if __name__ == '__main__':
    try:
        connection = connect()
        cur = connection.cursor()
        cur.execute('SELECT version()')
        printer = cur.fetchall()
        print(printer)
        cur.close()
        connection.close()
    except Exception as err:
        print(err)
