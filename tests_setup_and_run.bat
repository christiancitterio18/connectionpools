@echo off

echo Creating config files...


echo POSTGRES_USER=pgsql_ad >> ./.env
echo POSTGRES_PASSWORD=pgsql_ad >> ./.env
echo POSTGRES_DB=pgsql_connectingpool >> ./.env

echo [postgresql] >> ./database.ini
echo host=localhost >> ./database.ini
echo database=pgsql_connectingpool >> ./database.ini
echo user=pgsql_ad >> ./database.ini
echo password=pgsql_ad >> ./database.ini

echo Done!


echo Starting up db server...


start cmd /C docker-compose up

echo Waiting for db to start up...

timeout /T 60 /NOBREAK

echo "Creating test database, on database login password is the same as username..."

psql -U pgsql_ad -d pgsql_connectingpool -h localhost -p 5432 -f ./tests/utils/db_dump.sql
echo Done!


echo Starting unit tests...

python -m unittest discover -s ./tests/unit -t ./tests/unit
echo Unit tests done!


echo Starting Integration tests...

python -m unittest discover -s tests/integration -t tests/integration
echo Integration tests done!


echo Cleaning up...
docker-compose down
del database.ini
del .env